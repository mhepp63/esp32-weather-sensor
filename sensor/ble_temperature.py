# This example demonstrates a simple temperature sensor peripheral.
#
# The sensor's local value updates every second, and it will notify
# any connected central every 10 seconds.

import bluetooth
import random
import struct
import time
import esp32

import bmp280
from machine import Pin, I2C
from ble_advertising import advertising_payload
from micropython import const


_IRQ_CENTRAL_CONNECT = const(1)
_IRQ_CENTRAL_DISCONNECT = const(2)
_IRQ_GATTS_INDICATE_DONE = const(20)

_FLAG_READ = const(0x0002)
_FLAG_NOTIFY = const(0x0010)
_FLAG_INDICATE = const(0x0020)

# org.bluetooth.service.environmental_sensing
_ENV_SENSE_UUID = bluetooth.UUID(0x181A)
# org.bluetooth.characteristic.temperature
_TEMP_CHAR = (
    bluetooth.UUID(0x2A6E),
    _FLAG_READ | _FLAG_NOTIFY | _FLAG_INDICATE,
)

# org.bluetooth.characteristic.pressure
_PRESS_CHAR = (
    bluetooth.UUID(0x2A6D),
    _FLAG_READ | _FLAG_NOTIFY | _FLAG_INDICATE,
)

# org.bluetooth.characteristic.humidity
_HUMI_CHAR = (
    bluetooth.UUID(0x2A6F),
    _FLAG_READ | _FLAG_NOTIFY | _FLAG_INDICATE,
)

_ENV_SENSE_SERVICE = (
    _ENV_SENSE_UUID,
    (_TEMP_CHAR, _PRESS_CHAR, _HUMI_CHAR),
)

# org.bluetooth.characteristic.gap.appearance.xml
_ADV_APPEARANCE_GENERIC_THERMOMETER = const(768)


class BLETemperature:
    def __init__(self, ble, name="mpy-temp"):
        self._ble = ble
        self._ble.active(True)
        self._ble.irq(self._irq)
        x = self._ble.gatts_register_services((_ENV_SENSE_SERVICE,))
        print(x)
        self._handle_temp = x[0][0]
        self._handle_press = x[0][1]
        self._handle_humi = x[0][2]
        self._connections = set()
        self._payload = advertising_payload(
            name=name, services=[_ENV_SENSE_UUID], appearance=_ADV_APPEARANCE_GENERIC_THERMOMETER
        )
        self._advertise()

    def _irq(self, event, data):
        # Track connections so we can send notifications.
        if event == _IRQ_CENTRAL_CONNECT:
            conn_handle, _, _ = data
            self._connections.add(conn_handle)
        elif event == _IRQ_CENTRAL_DISCONNECT:
            conn_handle, _, _ = data
            self._connections.remove(conn_handle)
            # Start advertising again to allow a new connection.
            self._advertise()
        elif event == _IRQ_GATTS_INDICATE_DONE:
            conn_handle, value_handle, status = data

    def set_temperature(self, temp_deg_c,  notify=False, indicate=False):
        # Data is sint16 in degrees Celsius with a resolution of 0.01 degrees Celsius.
        # Write the local value, ready for a central to read.
        self._ble.gatts_write(self._handle_temp, struct.pack("<h", int(temp_deg_c * 100)))
        if notify or indicate:
            for conn_handle in self._connections:
                if notify:
                    # Notify connected centrals.
                    self._ble.gatts_notify(conn_handle, self._handle_temp)
                if indicate:
                    # Indicate connected centrals.
                    self._ble.gatts_indicate(conn_handle, self._handle_temp)

    def set_pressure(self, pressure_pascal,  notify=False, indicate=False):
        # Data is sint16 in degrees Celsius with a resolution of 0.01 degrees Celsius.
        # Write the local value, ready for a central to read.
        self._ble.gatts_write(self._handle_press, struct.pack("<l", int(pressure_pascal*10)))
        if notify or indicate:
            for conn_handle in self._connections:
                if notify:
                    # Notify connected centrals.
                    self._ble.gatts_notify(conn_handle, self._handle_press)
                if indicate:
                    # Indicate connected centrals.
                    self._ble.gatts_indicate(conn_handle, self._handle_press)

    def set_humidity(self, humidity,  notify=False, indicate=False):
        # Data is sint16 in 
        # Write the local value, ready for a central to read.
        self._ble.gatts_write(self._handle_humi, struct.pack("<h", int(humidity*100)))
        if notify or indicate:
            for conn_handle in self._connections:
                if notify:
                    # Notify connected centrals.
                    self._ble.gatts_notify(conn_handle, self._handle_humi)
                if indicate:
                    # Indicate connected centrals.
                    self._ble.gatts_indicate(conn_handle, self._handle_humi)


    def _advertise(self, interval_us=500000):
        self._ble.gap_advertise(interval_us, adv_data=self._payload)


def demo():
    ble = bluetooth.BLE()
    sensor = BLETemperature(ble)

    i2c = I2C(0)
    bmp = bmp280.BMP280(i2c)
    
    h = 60

    while True:

        t = bmp.temperature()
        p = bmp.pressure()
        h += random.uniform(-2, 2)

        print(t, p, h)
        sensor.set_temperature(t, notify=i == 0, indicate=False)
        sensor.set_pressure(p, notify=i == 0, indicate=False)
        sensor.set_humidity(h, notify=i == 0, indicate=False)
        time.sleep_ms(10000)


if __name__ == "__main__":
    demo()
